package com.capgemini.chess.board;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.figure.impl.Bishop;
import com.capgemini.chess.figure.impl.King;
import com.capgemini.chess.figure.impl.Knight;
import com.capgemini.chess.figure.impl.Pawn;
import com.capgemini.chess.figure.impl.Queen;
import com.capgemini.chess.figure.impl.Rook;

public class Board {

	private Box[][] board = new Box[8][8];

	public Board() {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				board[x][y] = new Box(x, y);
			}
		}
	}

	public void initializeFigures() {
		for (int i = 0; i < board.length; i++) {
			board[1][i].setFigure(new Pawn(Colour.BLACK, board[1][i]));
			board[6][i].setFigure(new Pawn(Colour.WHITE, board[6][i]));
		}
		board[0][0].setFigure(new Rook(Colour.BLACK, board[0][0]));
		board[0][7].setFigure(new Rook(Colour.BLACK, board[0][7]));
		board[0][1].setFigure(new Knight(Colour.BLACK, board[0][1]));
		board[0][6].setFigure(new Knight(Colour.BLACK, board[0][6]));
		board[0][2].setFigure(new Bishop(Colour.BLACK, board[0][2]));
		board[0][5].setFigure(new Bishop(Colour.BLACK, board[0][5]));
		board[0][3].setFigure(new Queen(Colour.BLACK, board[0][3]));
		board[0][4].setFigure(new King(Colour.BLACK, board[0][4]));
		board[7][0].setFigure(new Rook(Colour.WHITE, board[7][0]));
		board[7][7].setFigure(new Rook(Colour.WHITE, board[7][7]));
		board[7][1].setFigure(new Knight(Colour.WHITE, board[7][1]));
		board[7][6].setFigure(new Knight(Colour.WHITE, board[7][6]));
		board[7][2].setFigure(new Bishop(Colour.WHITE, board[7][2]));
		board[7][5].setFigure(new Bishop(Colour.WHITE, board[7][5]));
		board[7][3].setFigure(new Queen(Colour.WHITE, board[7][3]));
		board[7][4].setFigure(new King(Colour.WHITE, board[7][4]));
	}

	public Box[][] getBoard() {
		return board;
	}
}
