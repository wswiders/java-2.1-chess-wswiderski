package com.capgemini.chess.board.box;

import com.capgemini.chess.figure.Figure;

public class Box {

	private final int x;
	private final int y;
	private Figure figure;

	public Box(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Box(int x, int y, Figure figure) {
		this.x = x;
		this.y = y;
		this.figure = figure;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Box other = (Box) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Figure getFigure() {
		return figure;
	}

	public void setFigure(Figure figure) {
		this.figure = figure;
	}

	public void removeFigureFromBox() {
		this.figure = null;
	}

	public boolean isOccupied() {
		return figure != null;
	}
}
