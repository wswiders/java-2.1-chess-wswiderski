package com.capgemini.chess.figure;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;

public abstract class Figure {

	protected Colour colour;
	protected int numberOfMoves;
	private List<Box> posiblePosition = new ArrayList<Box>();
	private List<Box> posibleToBeat = new ArrayList<Box>();
	private Box startPosition;

	public Figure(Colour colour, Box startBox) {
		numberOfMoves = 0;
		this.colour = colour;
		this.startPosition = startBox;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colour == null) ? 0 : colour.hashCode());
		result = prime * result + ((startPosition == null) ? 0 : startPosition.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Figure other = (Figure) obj;
		if (colour != other.colour)
			return false;
		if (startPosition == null) {
			if (other.startPosition != null)
				return false;
		} else if (!startPosition.equals(other.startPosition))
			return false;
		return true;
	}

	public boolean isMoveValid(int xStart, int yStart, int xEnd, int yEnd) {
		if (xStart == xEnd && yStart == yEnd) {
			return false;
		}
		if (xStart < 0 || xStart > 7 || yStart < 0 || yStart > 7 || xEnd < 0 || xEnd > 7 || yEnd < 0 || yEnd > 7) {
			return false;
		}
		return true;
	}

	public List<Box> getPosiblePosition() {
		return posiblePosition;
	}

	public void setPossiblePosition(List<Box> posiblePosition) {
		this.posiblePosition = posiblePosition;
	}

	public int getNumberOfMoves() {
		return numberOfMoves;
	}

	public void addNumberOfMoves() {
		numberOfMoves++;
	}

	public void deleteNumberOfMoves() {
		numberOfMoves--;
	}

	public Colour getColour() {
		return colour;
	}

	public List<Box> getPosibleToBeat() {
		return posibleToBeat;
	}

	public void setPossibleToBeat(List<Box> posibleToBeat) {
		this.posibleToBeat = posibleToBeat;
	}
}
