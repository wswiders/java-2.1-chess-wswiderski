package com.capgemini.chess.figure.impl;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.Figure;
import com.capgemini.chess.figure.color.Colour;

public class King extends Figure {

	public King(Colour colour, Box startBox) {
		super(colour, startBox);
	}

	@Override
	public boolean isMoveValid(int xStart, int yStart, int xEnd, int yEnd) {
		if (!super.isMoveValid(xStart, yStart, xEnd, yEnd)) {
			return false;
		}
		if ((Math.abs(xStart - xEnd) == 0 && Math.abs(yStart - yEnd) == 1)
				|| (Math.abs(xStart - xEnd) == 1 && Math.abs(yStart - yEnd) == 0)
				|| (Math.abs(xStart - xEnd) == 1 && Math.abs(yStart - yEnd) == 1)) {
			return true;
		}
		return false;
	}
}
