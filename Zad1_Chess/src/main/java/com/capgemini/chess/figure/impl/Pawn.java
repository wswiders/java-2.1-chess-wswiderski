package com.capgemini.chess.figure.impl;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.Figure;
import com.capgemini.chess.figure.color.Colour;

public class Pawn extends Figure {

	public Pawn(Colour colour, Box startBox) {
		super(colour, startBox);
	}

	@Override
	public boolean isMoveValid(int xStart, int yStart, int xEnd, int yEnd) {
		if (!super.isMoveValid(xStart, yStart, xEnd, yEnd)) {
			return false;
		}
		if (super.colour == Colour.BLACK) {
			if (isMoveBlackPawnCorrect(xStart, yStart, xEnd, yEnd)) {
				return true;
			}
		} else {
			if (isMoveWhitePawnCorrect(xStart, yStart, xEnd, yEnd)) {
				return true;
			}
		}
		return false;
	}

	private boolean isMoveWhitePawnCorrect(int xStart, int yStart, int xEnd, int yEnd) {
		if (super.numberOfMoves == 0) {
			if (xStart - xEnd == 2 && Math.abs(yStart - yEnd) == 0) {
				return true;
			}
		}
		if (xStart - xEnd == 1 || (xStart - xEnd == 1 && Math.abs(yStart - yEnd) == 1)) {
			return true;
		}
		return false;
	}

	private boolean isMoveBlackPawnCorrect(int xStart, int yStart, int xEnd, int yEnd) {
		if (super.numberOfMoves == 0) {
			if (xStart - xEnd == -2 && Math.abs(yStart - yEnd) == 0) {
				return true;
			}
		}
		if (xStart - xEnd == -1 || (xStart - xEnd == -1 && Math.abs(yStart - yEnd) == 1)) {
			return true;
		}
		return false;
	}
}
