package com.capgemini.chess.figure.impl;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.Figure;
import com.capgemini.chess.figure.color.Colour;

public class Queen extends Figure {

	public Queen(Colour colour, Box startBox) {
		super(colour, startBox);
	}

	@Override
	public boolean isMoveValid(int xStart, int yStart, int xEnd, int yEnd) {
		if (!super.isMoveValid(xStart, yStart, xEnd, yEnd)) {
			return false;
		}
		if (Math.abs(xStart - xEnd) == Math.abs(yStart - yEnd)) {
			return true;
		}
		if (xStart == xEnd || yStart == yEnd) {
			return true;
		}
		return false;
	}
}
