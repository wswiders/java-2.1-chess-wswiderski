package com.capgemini.chess.game;

import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.game.exception.InvalidMoveException;

public interface Chess {

	/**
	 * set up the empty board and color of first player
	 * @param colour color of player that starts game
	 */
	public void initGame(Colour colour);
	
	/**
	 * places figures in places on board 
	 * and set up information about possible moves for each figure
	 * @throws InvalidMoveException
	 */
	public void placeFigures() throws InvalidMoveException;
	
	/**
	 * move figures on board
	 * @param xStart vertical coordinate of start box to move
	 * @param yStart horizontal coordinate of start box to move
	 * @param xEnd vertical coordinate of end box to move
	 * @param yEnd horizontal coordinate of end box to move
	 * @throws InvalidMoveException
	 */
	public void move(int xStart, int yStart, int xEnd, int yEnd) throws InvalidMoveException;
	
	/**
	 * set up information about possible moves for each figure after move
	 * throw exception when move cause own check
	 * change actual player color
	 * @throws InvalidMoveException
	 */
	public void updateGameStatus() throws InvalidMoveException;
	
}
