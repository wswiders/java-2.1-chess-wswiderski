package com.capgemini.chess.game.impl;

import java.util.List;

import com.capgemini.chess.board.Board;
import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.Figure;
import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.figure.impl.King;
import com.capgemini.chess.game.Chess;
import com.capgemini.chess.game.exception.InvalidMoveException;
import com.capgemini.chess.game.servieces.CheckControllerService;
import com.capgemini.chess.game.servieces.FigureCountService;
import com.capgemini.chess.game.servieces.MoveValidationService;

public class ChessImpl implements Chess {

	private Board board = new Board();
	private Box[][] gameBoard;
	private Colour currentPlayerColour;
	private List<Box> boxWithWhiteFigures;
	private List<Box> boxWithBlackFigures;
	private int[] lastMove;
	private Figure lastVisitedBoxFigure;

	public void initGame(Colour colour) {
		this.currentPlayerColour = colour;
		this.setGameBoard(board.getBoard());
	}

	public void placeFigures() throws InvalidMoveException {
		board.initializeFigures();
		getFigureData();
	}

	public void updateGameStatus() throws InvalidMoveException {
		getFigureData();
		if (!CheckControllerService.isCheckAfterMove(this, currentPlayerColour)) {
			changeActualPlayer();
		} else {
			moveBack();
			throw new InvalidMoveException("Ruch powoduje wlasnego szacha.");
		}
	}

	public void move(int xStart, int yStart, int xEnd, int yEnd) throws InvalidMoveException {
		Box startBox = getGameBoard()[xStart][yStart];
		Box endBox = getGameBoard()[xEnd][yEnd];
		lastVisitedBoxFigure = endBox.getFigure();
		if (startBox.isOccupied()) {
			if (startBox.getFigure().getColour() != currentPlayerColour) {
				throw new InvalidMoveException("Ruch innego gracza.");
			}
			if (startBox.getFigure().getPosiblePosition().size() > 0
					&& startBox.getFigure().getPosiblePosition().contains(endBox)) {
				moveFigure(startBox, endBox);
			} else if (startBox.getFigure().getPosibleToBeat().size() > 0
					&& startBox.getFigure().getPosibleToBeat().contains(endBox)) {
				removeFigure(startBox, endBox);
				moveFigure(startBox, endBox);
			} else {
				throw new InvalidMoveException("Nieprawidlowy ruch.");
			}
			lastMove = new int[] { xStart, yStart, xEnd, yEnd };
		} else {
			throw new InvalidMoveException("Nie mozna wykonac ruchu. Pole jest puste.");
		}
	}

	public List<Box> getBoxWithWhiteFigures() {
		return boxWithWhiteFigures;
	}

	public void setBoxWithWhiteFigures(List<Box> boxWithWhiteFigures) {
		this.boxWithWhiteFigures = boxWithWhiteFigures;
	}

	public List<Box> getBoxWithBlackFigures() {
		return boxWithBlackFigures;
	}

	public void setBoxWithBlackFigures(List<Box> boxWithBlackFigures) {
		this.boxWithBlackFigures = boxWithBlackFigures;
	}

	public Box[][] getGameBoard() {
		return gameBoard;
	}

	public void setGameBoard(Box[][] gameBoard) {
		this.gameBoard = gameBoard;
	}

	private void moveBack() {
		Box startBox = gameBoard[lastMove[2]][lastMove[3]];
		Box endBox = gameBoard[lastMove[0]][lastMove[1]];
		startBox.getFigure().deleteNumberOfMoves();
		endBox.setFigure(startBox.getFigure());
		startBox.setFigure(lastVisitedBoxFigure);
	}

	private void getFigureData() {
		FigureCountService.setCountOfFigures(this, getGameBoard());
		MoveValidationService.setPossibleMovesForAll(this, getGameBoard());
	}

	private void removeFigure(Box startBox, Box endBox) throws InvalidMoveException {
		if (!(endBox.getFigure() instanceof King)) {
			endBox.removeFigureFromBox();
		} else {
			throw new InvalidMoveException("Nie mozna zbic krola.");
		}
	}

	private void moveFigure(Box startBox, Box endBox) {
		startBox.getFigure().addNumberOfMoves();
		endBox.setFigure(startBox.getFigure());
		startBox.removeFigureFromBox();
	}

	private void changeActualPlayer() {
		currentPlayerColour = currentPlayerColour == Colour.WHITE ? Colour.BLACK : Colour.WHITE;
	}
}
