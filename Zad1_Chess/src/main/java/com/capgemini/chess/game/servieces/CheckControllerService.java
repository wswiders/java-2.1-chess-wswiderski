package com.capgemini.chess.game.servieces;

import java.util.List;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.figure.impl.King;
import com.capgemini.chess.game.impl.ChessImpl;

public class CheckControllerService {

	public static boolean isCheckAfterMove(ChessImpl chess, Colour colour) {
		List<Box> enemyFigures;
		List<Box> alliesFigures;
		if (colour == Colour.BLACK) {
			enemyFigures = chess.getBoxWithWhiteFigures();
			alliesFigures = chess.getBoxWithBlackFigures();
		} else {
			enemyFigures = chess.getBoxWithBlackFigures();
			alliesFigures = chess.getBoxWithWhiteFigures();
		}

		for (Box kingBox : alliesFigures) {
			if (kingBox.getFigure() instanceof King) {
				for (Box enemyBox : enemyFigures) {
					if (enemyBox.getFigure().getPosibleToBeat().contains(kingBox)) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
