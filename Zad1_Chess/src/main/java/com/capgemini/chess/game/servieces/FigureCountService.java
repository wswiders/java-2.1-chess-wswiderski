package com.capgemini.chess.game.servieces;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.game.impl.ChessImpl;

public class FigureCountService {

	public static void setCountOfFigures(ChessImpl chess, Box[][] gameBoard) {
		chess.setBoxWithBlackFigures(getBlackFigures(gameBoard));
		chess.setBoxWithWhiteFigures(getWhiteFigures(gameBoard));
	}

	private static List<Box> getWhiteFigures(Box[][] gameBoard) {
		List<Box> whiteFigures = new ArrayList<Box>();
		for (Box[] boxs : gameBoard) {
			for (Box box : boxs) {
				if (box.isOccupied() && box.getFigure().getColour() == Colour.WHITE) {
					whiteFigures.add(box);
				}
			}
		}
		return whiteFigures;
	}

	private static List<Box> getBlackFigures(Box[][] gameBoard) {
		List<Box> blackFigures = new ArrayList<Box>();
		for (Box[] boxs : gameBoard) {
			for (Box box : boxs) {
				if (box.isOccupied() && box.getFigure().getColour() == Colour.BLACK) {
					blackFigures.add(box);
				}
			}
		}
		return blackFigures;
	}
}
