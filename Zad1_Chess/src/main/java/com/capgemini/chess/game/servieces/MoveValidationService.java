package com.capgemini.chess.game.servieces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.impl.Knight;
import com.capgemini.chess.figure.impl.Pawn;
import com.capgemini.chess.game.impl.ChessImpl;

public class MoveValidationService {

	public static void setPossibleMovesForAll(ChessImpl chess, Box[][] gameBoard) {
		List<Box> figures = new ArrayList<Box>();
		figures.addAll(chess.getBoxWithBlackFigures());
		figures.addAll(chess.getBoxWithWhiteFigures());
		for (Box box : figures) {
			setPossibleMoves(box, gameBoard);
		}
	}

	protected static void setPossibleMoves(Box currentBox, Box[][] gameBoad) {
		if (currentBox.getFigure() instanceof Pawn) {
			setPawnsMoves(currentBox, gameBoad);
		} else if (currentBox.getFigure() instanceof Knight) {
			setKnightsMoves(currentBox, gameBoad);
		} else {
			setMovesForOtherFigures(currentBox, gameBoad);
		}
	}

	private static void setPawnsMoves(Box currentBox, Box[][] gameBoard) {
		List<Box> possiblePosition = getPossibleMovesForPawn(currentBox, gameBoard);
		List<Box> enemy = getPossibleToBeatForPawn(currentBox, gameBoard);
		List<Box> allies = getAlliesForPawn(currentBox, gameBoard);
		List<Box> updatePossiblePosition = updatePossibleMovesForPawn(currentBox, possiblePosition, allies);
		currentBox.getFigure().setPossiblePosition(updatePossiblePosition);
		currentBox.getFigure().setPossibleToBeat(enemy);
	}

	private static void setKnightsMoves(Box currentBox, Box[][] gameBoard) {
		List<Box> possiblePosition = getPossibleMoves(currentBox, gameBoard);
		List<Box> enemy = getPossibleToBeat(currentBox, gameBoard);
		currentBox.getFigure().setPossiblePosition(possiblePosition);
		currentBox.getFigure().setPossibleToBeat(enemy);
	}

	private static void setMovesForOtherFigures(Box currentBox, Box[][] gameBoard) {
		List<Box> possiblePosition = getPossibleMoves(currentBox, gameBoard);
		List<Box> enemy = getPossibleToBeat(currentBox, gameBoard);
		List<Box> allies = getAllies(currentBox, gameBoard);
		List<Box> updPossiblePos = updatePossibleMoves(currentBox, possiblePosition, enemy, allies);
		List<Box> updPossibleToBeat = updatePossileToBeat(currentBox, enemy, allies);
		currentBox.getFigure().setPossiblePosition(updPossiblePos);
		currentBox.getFigure().setPossibleToBeat(updPossibleToBeat);
	}

	private static List<Box> getPossibleMovesForPawn(Box currentBox, Box[][] gameBoard) {
		List<Box> possiblePosition = new ArrayList<Box>();
		for (int x = 0; x < gameBoard.length; x++) {
			for (int y = 0; y < gameBoard[0].length; y++) {
				if (isMoveValidForFigure(currentBox, x, y)
						&& Math.abs(currentBox.getY() - gameBoard[x][y].getY()) == 0) {
					if (!gameBoard[x][y].isOccupied()) {
						possiblePosition.add(gameBoard[x][y]);
					}
				}
			}
		}
		return possiblePosition;
	}

	private static List<Box> getPossibleMoves(Box currentBox, Box[][] gameBoard) {
		List<Box> possiblePosition = new ArrayList<Box>();
		for (int x = 0; x < gameBoard.length; x++) {
			for (int y = 0; y < gameBoard[0].length; y++) {
				if (isMoveValidForFigure(currentBox, x, y)) {
					if (!gameBoard[x][y].isOccupied()) {
						possiblePosition.add(gameBoard[x][y]);
					}
				}
			}
		}
		return possiblePosition;
	}

	private static List<Box> getAlliesForPawn(Box currentBox, Box[][] gameBoard) {
		List<Box> possiblePosition = new ArrayList<Box>();
		for (int x = 0; x < gameBoard.length; x++) {
			for (int y = 0; y < gameBoard[0].length; y++) {
				if (isMoveValidForFigure(currentBox, x, y)
						&& Math.abs(currentBox.getY() - gameBoard[x][y].getY()) == 0) {
					if (gameBoard[x][y].isOccupied()) {
						possiblePosition.add(gameBoard[x][y]);
					}
				}
			}
		}
		return possiblePosition;
	}

	private static List<Box> getAllies(Box currentBox, Box[][] gameBoard) {
		List<Box> possibleToBeat = new ArrayList<Box>();
		for (int x = 0; x < gameBoard.length; x++) {
			for (int y = 0; y < gameBoard[0].length; y++) {
				if (isMoveValidForFigure(currentBox, x, y)) {
					if (gameBoard[x][y].isOccupied() && isColourSame(currentBox, gameBoard, x, y)) {
						possibleToBeat.add(gameBoard[x][y]);
					}
				}
			}
		}
		return possibleToBeat;
	}

	private static List<Box> getPossibleToBeatForPawn(Box currentBox, Box[][] gameBoard) {
		List<Box> possibleToBeat = new ArrayList<Box>();
		for (int x = 0; x < gameBoard.length; x++) {
			for (int y = 0; y < gameBoard[0].length; y++) {
				if (isMoveValidForFigure(currentBox, x, y)
						&& Math.abs(currentBox.getY() - gameBoard[x][y].getY()) == 1) {
					if (gameBoard[x][y].isOccupied() && isColourDifferent(currentBox, gameBoard, x, y)) {
						possibleToBeat.add(gameBoard[x][y]);
					}
				}
			}
		}
		return possibleToBeat;
	}

	private static List<Box> getPossibleToBeat(Box currentBox, Box[][] gameBoard) {
		List<Box> possibleToBeat = new ArrayList<Box>();
		for (int x = 0; x < gameBoard.length; x++) {
			for (int y = 0; y < gameBoard[0].length; y++) {
				if (isMoveValidForFigure(currentBox, x, y)) {
					if (gameBoard[x][y].isOccupied() && isColourDifferent(currentBox, gameBoard, x, y)) {
						possibleToBeat.add(gameBoard[x][y]);
					}
				}
			}
		}
		return possibleToBeat;
	}

	private static List<Box> updatePossibleMovesForPawn(Box currentBox, List<Box> possiblePosition,
			List<Box> possibleToBeat) {
		List<Box> forbitenBoxes = new ArrayList<Box>();
		List<Box> updatedPossibleBoxes = new ArrayList<Box>(possiblePosition);
		forbitenBoxes.addAll(possibleToBeat);
		for (Box possible : possiblePosition) {
			for (Box obsticle : forbitenBoxes) {
				if (isObsticleWithin(currentBox, possible, obsticle)) {
					updatedPossibleBoxes.remove(possible);
				}
			}
		}
		return updatedPossibleBoxes;
	}

	private static List<Box> updatePossibleMoves(Box currentBox, List<Box> possiblePosition, List<Box> possibleToBeat,
			List<Box> allies) {
		List<Box> forbitenBoxes = new ArrayList<Box>();
		List<Box> updatedPossibleBoxes = new ArrayList<Box>(possiblePosition);
		forbitenBoxes.addAll(possibleToBeat);
		forbitenBoxes.addAll(allies);
		for (Box possible : possiblePosition) {
			for (Box obsticle : forbitenBoxes) {
				if (isObsticleWithin(currentBox, possible, obsticle) && areLinear(currentBox, possible, obsticle)) {
					updatedPossibleBoxes.remove(possible);
				}
			}
		}
		return updatedPossibleBoxes;
	}

	private static List<Box> updatePossileToBeat(Box currentBox, List<Box> possibleToBeat, List<Box> allies) {
		List<Box> forbitenBoxes = new ArrayList<Box>();
		List<Box> updatedPossibleToBeat = new ArrayList<Box>(possibleToBeat);
		forbitenBoxes.addAll(possibleToBeat);
		forbitenBoxes.addAll(allies);
		for (Box possible : possibleToBeat) {
			if (!isNeighbour(currentBox, possible)) {
				for (Box obsticle : forbitenBoxes) {
					if (!possible.equals(obsticle) && isObsticleWithin(currentBox, possible, obsticle)) {
						updatedPossibleToBeat.remove(possible);
					}
				}
			}
		}
		return updatedPossibleToBeat;
	}

	private static boolean isColourSame(Box currentBox, Box[][] gameBoard, int x, int y) {
		return gameBoard[x][y].getFigure().getColour() == currentBox.getFigure().getColour();
	}

	private static boolean isColourDifferent(Box currentBox, Box[][] gameBoard, int x, int y) {
		return gameBoard[x][y].getFigure().getColour() != currentBox.getFigure().getColour();
	}

	private static boolean isMoveValidForFigure(Box currentBox, int x, int y) {
		return currentBox.getFigure().isMoveValid(currentBox.getX(), currentBox.getY(), x, y);
	}

	private static boolean isNeighbour(Box startBox, Box endBox) {
		int xStart = startBox.getX();
		int yStart = startBox.getY();
		int xEnd = endBox.getX();
		int yEnd = endBox.getY();
		return Math.abs(xStart - xEnd) == 1 || Math.abs(yStart - yEnd) == 1;
	}

	private static boolean areLinear(Box startBox, Box endBox, Box middleBox) {
		int w1 = startBox.getX() * endBox.getY() + endBox.getX() * middleBox.getY()
				+ middleBox.getX() * startBox.getX();
		int w2 = middleBox.getX() * endBox.getY() + startBox.getX() * middleBox.getY()
				+ endBox.getX() * startBox.getY();
		return (w1 - w2) == 0;
	}

	private static boolean isObsticleWithin(Box startBox, Box endBox, Box middleBox) {
		List<Integer> x = new ArrayList<Integer>(Arrays.asList(startBox.getX(), endBox.getX()));
		List<Integer> y = new ArrayList<Integer>(Arrays.asList(startBox.getY(), endBox.getY()));
		int xMid = middleBox.getX();
		int yMid = middleBox.getY();
		return Collections.max(x) >= xMid && Collections.min(x) <= xMid && Collections.max(y) >= yMid
				&& Collections.min(y) <= yMid;
	}
}