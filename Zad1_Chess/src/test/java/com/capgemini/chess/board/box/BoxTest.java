package com.capgemini.chess.board.box;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.Figure;
import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.figure.impl.Pawn;

public class BoxTest {

	private static final Pawn WHITE_PAWN = new Pawn(Colour.WHITE, new Box(1, 1));
	private Box box;

	@Test
	public void shouldReturnTrueWhenFigureOnBox() {
		box = new Box(1, 1, WHITE_PAWN);
		boolean expected = box.isOccupied();
		assertTrue(expected);
	}

	@Test
	public void shouldReturnFalseWhenNoFigureOnBox() {
		box = new Box(1, 1);
		boolean expected = box.isOccupied();
		assertFalse(expected);
	}

	@Test
	public void shouldRemoveFigureFromBox() {
		box = new Box(1, 1, WHITE_PAWN);
		box.removeFigureFromBox();
		boolean expected = box.isOccupied();
		assertFalse(expected);
	}

	@Test
	public void shouldAddFigureToBox() {
		box = new Box(1, 1);
		box.setFigure(WHITE_PAWN);
		boolean expected = box.isOccupied();
		assertTrue(expected);
	}

	@Test
	public void shouldReplaceFigureOnBox() {
		box = new Box(1, 1, new Pawn(Colour.BLACK, new Box(1, 2)));
		box.setFigure(WHITE_PAWN);
		Figure expected = box.getFigure();
		assertEquals(WHITE_PAWN, expected);
	}

}
