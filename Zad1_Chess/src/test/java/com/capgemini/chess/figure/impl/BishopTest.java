package com.capgemini.chess.figure.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;

public class BishopTest {

	private Bishop bishop;

	@Before
	public void setUp() throws Exception {
		bishop = new Bishop(Colour.WHITE, new Box(1, 1));
	}

	@Test
	public void shouldReturnFalseWhenNoMove() {
		boolean actual = bishop.isMoveValid(0, 0, 0, 0);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveOutOfBoard() {
		boolean actual = bishop.isMoveValid(0, 0, 8, 8);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenStartPositionOutOfBoard() {
		boolean actual = bishop.isMoveValid(8, -21, 5, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveIsVertical() {
		boolean actual = bishop.isMoveValid(1, 1, 5, 1);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveIsHorizontal() {
		boolean actual = bishop.isMoveValid(1, 1, 1, 5);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveOblique() {
		boolean actual = bishop.isMoveValid(1, 1, 2, 2);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveOverTwoBoxLong() {
		boolean actual = bishop.isMoveValid(1, 1, 7, 7);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveIncorect() {
		boolean actual = bishop.isMoveValid(1, 1, 5, 7);
		assertFalse(actual);
	}
}
