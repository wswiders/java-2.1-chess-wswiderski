package com.capgemini.chess.figure.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;

public class KingTest {

	private King king;

	@Before
	public void setUp() throws Exception {
		king = new King(Colour.WHITE, new Box(1, 1));
	}

	@Test
	public void shouldReturnFalseWhenNoMove() {
		boolean actual = king.isMoveValid(0, 0, 0, 0);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveOutOfBoard() {
		boolean actual = king.isMoveValid(0, 0, 8, 8);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenStartPositionOutOfBoard() {
		boolean actual = king.isMoveValid(8, -21, 5, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveCorrect() {
		boolean actual = king.isMoveValid(1, 1, 2, 2);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveDown() {
		boolean actual = king.isMoveValid(1, 1, 2, 1);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveUp() {
		boolean actual = king.isMoveValid(1, 1, 0, 1);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveLeft() {
		boolean actual = king.isMoveValid(1, 1, 1, 0);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveRight() {
		boolean actual = king.isMoveValid(1, 1, 1, 2);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveObliquely() {
		boolean actual = king.isMoveValid(1, 1, 0, 0);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveOverTwoBox() {
		boolean actual = king.isMoveValid(1, 1, 3, 1);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveIncorrect() {
		boolean actual = king.isMoveValid(1, 1, 6, 1);
		assertFalse(actual);
	}
}
