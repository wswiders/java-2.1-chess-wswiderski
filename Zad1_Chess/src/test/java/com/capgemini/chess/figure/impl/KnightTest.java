package com.capgemini.chess.figure.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;

public class KnightTest {

	private Knight knight;

	@Before
	public void setUp() throws Exception {
		knight = new Knight(Colour.WHITE, new Box(1, 1));
	}

	@Test
	public void shouldReturnFalseWhenNoMove() {
		boolean actual = knight.isMoveValid(0, 0, 0, 0);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveOutOfBoard() {
		boolean actual = knight.isMoveValid(0, 0, 8, 8);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenStartPositionOutOfBoard() {
		boolean actual = knight.isMoveValid(8, -21, 5, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveIsIncorrect() {
		boolean actual = knight.isMoveValid(1, 1, 2, 5);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveIsCorrect() {
		boolean actual = knight.isMoveValid(1, 1, 3, 2);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveUp() {
		boolean actual = knight.isMoveValid(5, 1, 3, 2);
		assertTrue(actual);
	}
}
