package com.capgemini.chess.figure.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;

public class PawnTest {

	private Pawn whitePawn;
	private Pawn blackPawn;

	@Before
	public void setUp() throws Exception {
		whitePawn = new Pawn(Colour.WHITE, new Box(1, 1));
		blackPawn = new Pawn(Colour.BLACK, new Box(1, 2));
	}

	@Test
	public void shouldReturnFalseWhenWhitePawnNoMove() {
		boolean actual = whitePawn.isMoveValid(0, 0, 0, 0);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenWhitePawnMoveOutOfBoard() {
		boolean actual = whitePawn.isMoveValid(0, 0, 8, 8);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenWhitePawnStartPositionOutOfBoard() {
		boolean actual = whitePawn.isMoveValid(8, -21, 5, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenBlackPawnNoMove() {
		boolean actual = blackPawn.isMoveValid(0, 0, 0, 0);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenBlackPawnMoveOutOfBoard() {
		boolean actual = blackPawn.isMoveValid(0, 0, 8, 8);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenBlackPawnStartPositionOutOfBoard() {
		boolean actual = blackPawn.isMoveValid(8, -21, 5, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenWhitePawnMoveDown() {
		boolean actual = whitePawn.isMoveValid(1, 1, 2, 1);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenWhitePawnMoveUp() {
		boolean actual = whitePawn.isMoveValid(1, 1, 0, 1);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenWhitePawnDoDoubleMoveFirstTime() {
		boolean actual = whitePawn.isMoveValid(6, 1, 4, 1);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnFalseWhenWhitePawnDoDoubleMoveAfterMove() {
		whitePawn.addNumberOfMoves();
		boolean actual = whitePawn.isMoveValid(6, 1, 4, 1);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenWhitePawnMoveHorizontal() {
		whitePawn.addNumberOfMoves();
		boolean actual = whitePawn.isMoveValid(6, 1, 6, 2);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenWhitePawnDoAtackMove() {
		whitePawn.addNumberOfMoves();
		boolean actual = whitePawn.isMoveValid(6, 1, 5, 2);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnFalseWhenWhitePawnMoveIncorrect() {
		boolean actual = whitePawn.isMoveValid(6, 1, 4, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenBlackPawnMoveDown() {
		boolean actual = blackPawn.isMoveValid(1, 1, 2, 1);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnFalseWhenWhitePawnMoveUp() {
		boolean actual = blackPawn.isMoveValid(1, 1, 0, 1);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenBlackPawnDoDoubleMoveFirstTime() {
		boolean actual = blackPawn.isMoveValid(1, 1, 3, 1);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnFalseWhenBlackPawnDoDoubleMoveAfterMove() {
		blackPawn.addNumberOfMoves();
		boolean actual = blackPawn.isMoveValid(1, 1, 3, 1);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenBlackPawnMoveHorizontal() {
		whitePawn.addNumberOfMoves();
		boolean actual = blackPawn.isMoveValid(6, 1, 6, 2);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenBlackPawnDoAtackMove() {
		whitePawn.addNumberOfMoves();
		boolean actual = blackPawn.isMoveValid(4, 1, 5, 2);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnFalseWhenBlackPawnMoveIncorrect() {
		boolean actual = blackPawn.isMoveValid(2, 1, 4, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenBlackPawnDoAtackLeft() {
		boolean actual = blackPawn.isMoveValid(2, 1, 3, 0);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenWhitePawnDoAtackLeft() {
		boolean actual = whitePawn.isMoveValid(4, 1, 3, 0);
		assertTrue(actual);
	}
}
