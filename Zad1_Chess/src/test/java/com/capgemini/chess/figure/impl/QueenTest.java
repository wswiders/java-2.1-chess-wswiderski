package com.capgemini.chess.figure.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;

public class QueenTest {

	private Queen queen;

	@Before
	public void setUp() throws Exception {
		queen = new Queen(Colour.WHITE, new Box(1, 1));
	}

	@Test
	public void shouldReturnFalseWhenNoMove() {
		boolean actual = queen.isMoveValid(0, 0, 0, 0);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveOutOfBoard() {
		boolean actual = queen.isMoveValid(0, 0, 8, 8);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenStartPositionOutOfBoard() {
		boolean actual = queen.isMoveValid(8, -21, 5, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveIsIncorrect() {
		boolean actual = queen.isMoveValid(1, 1, 5, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveVeertical() {
		boolean actual = queen.isMoveValid(1, 1, 2, 1);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveHorizontal() {
		boolean actual = queen.isMoveValid(1, 1, 1, 2);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveObliquely() {
		boolean actual = queen.isMoveValid(1, 1, 0, 0);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveOverTwoBox() {
		boolean actual = queen.isMoveValid(1, 1, 5, 5);
		assertTrue(actual);
	}
}
