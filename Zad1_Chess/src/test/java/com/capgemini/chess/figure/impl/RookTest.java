package com.capgemini.chess.figure.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;

public class RookTest {

	private Rook rook;

	@Before
	public void setUp() throws Exception {
		rook = new Rook(Colour.WHITE, new Box(1, 1));
	}

	@Test
	public void shouldReturnFalseWhenNoMove() {
		boolean actual = rook.isMoveValid(0, 0, 0, 0);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveOutOfBoard() {
		boolean actual = rook.isMoveValid(0, 0, 8, 8);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenStartPositionOutOfBoard() {
		boolean actual = rook.isMoveValid(8, -21, 5, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnFalseWhenMoveIncorrect() {
		boolean actual = rook.isMoveValid(1, 1, 3, 3);
		assertFalse(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveVertical() {
		boolean actual = rook.isMoveValid(1, 1, 2, 1);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveHorizontal() {
		boolean actual = rook.isMoveValid(1, 1, 1, 2);
		assertTrue(actual);
	}

	@Test
	public void shouldReturnTrueWhenMoveOverTwoBox() {
		boolean actual = rook.isMoveValid(1, 1, 3, 1);
		assertTrue(actual);
	}
}
