package com.capgemini.chess.game.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.Figure;
import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.figure.impl.King;
import com.capgemini.chess.figure.impl.Pawn;
import com.capgemini.chess.figure.impl.Queen;
import com.capgemini.chess.figure.impl.Rook;
import com.capgemini.chess.game.exception.InvalidMoveException;
import com.capgemini.chess.game.servieces.FigureCountService;
import com.capgemini.chess.game.servieces.MoveValidationService;

public class ChessImplTest {

	private static final Pawn BLACK_PAWN = new Pawn(Colour.BLACK, new Box(2, 1));
	private static final King WHITE_KING = new King(Colour.WHITE, new Box(1, 5));
	private static final King BLACK_KING = new King(Colour.BLACK, new Box(6, 7));
	private static final King BLACK_KING_MOVE = new King(Colour.BLACK, new Box(7, 7));
	private static final Pawn WHITE_PAWN = new Pawn(Colour.WHITE, new Box(1, 1));
	private static final Rook WHITE_ROOK = new Rook(Colour.WHITE, new Box(1, 3));
	private static final Queen WHITE_QUEEN = new Queen(Colour.WHITE, new Box(4, 1));
	private static final Queen BLACK_QUEEN = new Queen(Colour.BLACK, new Box(1, 2));
	private ChessImpl game = new ChessImpl();
	private Box[][] gameBoard;

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
		game.initGame(Colour.WHITE);
		gameBoard = game.getGameBoard();
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenTryToMoveEmptyBox() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Nie mozna wykonac ruchu. Pole jest puste.");
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(1, 1, 2, 2);
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenNotPlayerTurn() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Ruch innego gracza.");
		gameBoard[0][0].setFigure(BLACK_QUEEN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 2, 2);
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenMoveIncorrect() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Nieprawidlowy ruch.");
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 2, 3);
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenMoveIsDistrubed() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Nieprawidlowy ruch.");
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[1][1].setFigure(WHITE_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 3, 3);
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenTryToBeatKing() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Nie mozna zbic krola.");
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[1][1].setFigure(BLACK_KING);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 1, 1);
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenTryToBeatAllies() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Nieprawidlowy ruch.");
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[1][1].setFigure(WHITE_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 1, 1);
	}

	@Test
	public void shouldAllowBeatEnemy() throws InvalidMoveException {
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[1][1].setFigure(new Pawn(Colour.BLACK, new Box(1, 2)));
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 1, 1);
		Figure actual = game.getGameBoard()[1][1].getFigure();
		assertEquals(WHITE_QUEEN, actual);
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenTryToMoveAgain() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Ruch innego gracza.");
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[1][1].setFigure(WHITE_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 3, 0);
		game.updateGameStatus();
		game.move(3, 0, 0, 0);
	}

	@Test
	public void shouldAddNumberOfMovesAfterMove() throws InvalidMoveException {
		gameBoard[0][0].setFigure(WHITE_ROOK);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 3, 0);
		int actual = game.getGameBoard()[3][0].getFigure().getNumberOfMoves();
		assertEquals(1, actual);
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenMoveCauseCheck() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Ruch powoduje wlasnego szacha.");
		gameBoard[1][2].setFigure(WHITE_QUEEN);
		gameBoard[0][2].setFigure(WHITE_KING);
		gameBoard[5][2].setFigure(BLACK_QUEEN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(1, 2, 1, 5);
		game.updateGameStatus();
	}

	@Test
	public void shouldNotThrowInvalidMoveExceptionWhenMoveCauseCheck() throws InvalidMoveException {
		gameBoard[1][2].setFigure(WHITE_QUEEN);
		gameBoard[0][2].setFigure(WHITE_KING);
		gameBoard[5][3].setFigure(BLACK_QUEEN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(1, 2, 1, 5);
		game.updateGameStatus();
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenNoneMovePosible() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Nieprawidlowy ruch.");
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[0][1].setFigure(BLACK_PAWN);
		gameBoard[1][1].setFigure(BLACK_PAWN);
		gameBoard[1][0].setFigure(BLACK_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 2, 2);
		game.updateGameStatus();
	}

	@Test
	public void shouldThrowInvalidMoveExceptionWhenNotExcapeCheck() throws InvalidMoveException {
		exception.expect(InvalidMoveException.class);
		exception.expectMessage("Ruch powoduje wlasnego szacha.");
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[0][1].setFigure(WHITE_ROOK);
		gameBoard[7][1].setFigure(BLACK_KING);
		gameBoard[6][0].setFigure(BLACK_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 6, 0);
		game.updateGameStatus();
		game.move(7, 1, 7, 0);
		game.updateGameStatus();
	}

	@Test
	public void shouldNotThrowInvalidMoveExceptionWhenEscapeCheck() throws InvalidMoveException {
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[0][1].setFigure(WHITE_ROOK);
		gameBoard[7][1].setFigure(BLACK_KING);
		gameBoard[6][0].setFigure(BLACK_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 6, 0);
		game.updateGameStatus();
		game.move(7, 1, 7, 2);
		game.updateGameStatus();
	}

	@Test
	public void shouldUndoMoveWhenNotEscapeCheck() throws InvalidMoveException {
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[0][1].setFigure(WHITE_ROOK);
		gameBoard[7][1].setFigure(BLACK_KING);
		gameBoard[6][0].setFigure(BLACK_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 6, 0);
		game.updateGameStatus();
		game.move(7, 1, 7, 0);
		try {
			game.updateGameStatus();
		} catch (InvalidMoveException e) {
		}
		Figure actual = game.getGameBoard()[7][1].getFigure();
		assertEquals(BLACK_KING, actual);
	}

	@Test
	public void shouldBoxAfterUndouBeSameWhenNotEscapeCheck() throws InvalidMoveException {
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[0][1].setFigure(WHITE_ROOK);
		gameBoard[7][1].setFigure(BLACK_KING);
		gameBoard[6][0].setFigure(BLACK_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 6, 0);
		game.updateGameStatus();
		game.move(7, 1, 7, 0);
		try {
			game.updateGameStatus();
		} catch (InvalidMoveException e) {
		}
		Figure actual = game.getGameBoard()[7][0].getFigure();
		assertNull(actual);
	}

	@Test
	public void shouldFigureStaySameWhenNotEscapeCheck() throws InvalidMoveException {
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[0][1].setFigure(WHITE_ROOK);
		gameBoard[7][0].setFigure(WHITE_PAWN);
		gameBoard[7][1].setFigure(BLACK_KING);
		gameBoard[6][0].setFigure(BLACK_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 6, 0);
		game.updateGameStatus();
		game.move(7, 1, 7, 0);
		try {
			game.updateGameStatus();
		} catch (InvalidMoveException e) {
		}
		Figure actual = game.getGameBoard()[7][0].getFigure();
		assertEquals(WHITE_PAWN, actual);
	}

	@Test
	public void shouldNumberOfMovesStaySameWhenNotEscapeCheck() throws InvalidMoveException {
		gameBoard[0][0].setFigure(WHITE_QUEEN);
		gameBoard[0][1].setFigure(WHITE_ROOK);
		gameBoard[7][1].setFigure(BLACK_KING_MOVE);
		gameBoard[6][0].setFigure(BLACK_PAWN);
		game.setGameBoard(gameBoard);
		setRountInfo();
		game.move(0, 0, 6, 0);
		game.updateGameStatus();
		game.move(7, 1, 7, 0);
		try {
			game.updateGameStatus();
		} catch (InvalidMoveException e) {
		}
		int actual = game.getGameBoard()[7][1].getFigure().getNumberOfMoves();
		assertEquals(0, actual);
	}

	private void setRountInfo() {
		FigureCountService.setCountOfFigures(game, gameBoard);
		MoveValidationService.setPossibleMovesForAll(game, gameBoard);
	}
}
