package com.capgemini.chess.game.servieces;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.figure.impl.King;
import com.capgemini.chess.figure.impl.Queen;
import com.capgemini.chess.figure.impl.Rook;
import com.capgemini.chess.game.exception.InvalidMoveException;
import com.capgemini.chess.game.impl.ChessImpl;
import com.capgemini.chess.game.servieces.CheckControllerService;
import com.capgemini.chess.game.servieces.FigureCountService;
import com.capgemini.chess.game.servieces.MoveValidationService;

public class CheckControllerServiceTest {

	private static final Colour WHITE = Colour.WHITE;
	private static final Colour BLACK = Colour.BLACK;
	private static final Queen WHITE_QUEEN = new Queen(WHITE, new Box(1, 1));
	private static final Rook BLACK_ROOK = new Rook(BLACK, new Box(1, 2));
	private static final King BLACK_KING = new King(BLACK, new Box(1, 3));
	private static final Queen BLACK_QUEEN = new Queen(BLACK, new Box(1, 4));
	private static final Rook WHITE_ROOK = new Rook(WHITE, new Box(1, 5));
	private static final King WWHITE_KING = new King(WHITE, new Box(1, 6));
	private ChessImpl chess;
	private Box[][] gameBoard;

	@Before
	public void setUp() throws Exception {
		chess = new ChessImpl();
		chess.initGame(WHITE);
		gameBoard = chess.getGameBoard();
	}

	@Test
	public void sholudReturnTrueWhenMoveDoCheck() throws InvalidMoveException {
		gameBoard[5][3].setFigure(WWHITE_KING);
		gameBoard[4][3].setFigure(WHITE_ROOK);
		gameBoard[0][3].setFigure(BLACK_QUEEN);
		setRountInfo();
		chess.move(4, 3, 4, 1);
		setRountInfo();
		boolean actual = CheckControllerService.isCheckAfterMove(chess, WHITE);
		assertTrue(actual);
	}

	@Test
	public void sholudReturnFalseWhenMoveHideKing() throws InvalidMoveException {
		gameBoard[5][3].setFigure(WWHITE_KING);
		gameBoard[4][1].setFigure(WHITE_ROOK);
		gameBoard[0][3].setFigure(BLACK_QUEEN);
		setRountInfo();
		chess.move(4, 1, 4, 3);
		setRountInfo();
		boolean actual = CheckControllerService.isCheckAfterMove(chess, WHITE);
		assertFalse(actual);
	}

	@Test
	public void sholudReturnFalseWhenMoveDontShowKing() throws InvalidMoveException {
		gameBoard[5][3].setFigure(WWHITE_KING);
		gameBoard[4][3].setFigure(WHITE_ROOK);
		gameBoard[7][7].setFigure(WHITE_ROOK);
		gameBoard[0][3].setFigure(BLACK_QUEEN);
		setRountInfo();
		chess.move(7, 7, 0, 7);
		setRountInfo();
		boolean actual = CheckControllerService.isCheckAfterMove(chess, WHITE);
		assertFalse(actual);
	}

	@Test
	public void sholudReturnTrueWhenMoveDontHideKing() throws InvalidMoveException {
		gameBoard[5][3].setFigure(WWHITE_KING);
		gameBoard[4][1].setFigure(WHITE_ROOK);
		gameBoard[0][3].setFigure(BLACK_QUEEN);
		setRountInfo();
		chess.move(4, 1, 4, 2);
		setRountInfo();
		boolean actual = CheckControllerService.isCheckAfterMove(chess, WHITE);
		assertTrue(actual);
	}

	@Test
	public void sholudReturnFalseWhenBlackMoveDontShowKing() throws InvalidMoveException {
		chess.initGame(BLACK);
		gameBoard[5][3].setFigure(BLACK_KING);
		gameBoard[4][3].setFigure(BLACK_ROOK);
		gameBoard[7][7].setFigure(BLACK_ROOK);
		gameBoard[0][3].setFigure(WHITE_QUEEN);
		setRountInfo();
		chess.move(7, 7, 0, 7);
		setRountInfo();
		boolean actual = CheckControllerService.isCheckAfterMove(chess, BLACK);
		assertFalse(actual);
	}

	@Test
	public void sholudReturnTrueWhenBlackMoveDontHideKing() throws InvalidMoveException {
		chess.initGame(BLACK);
		gameBoard[5][3].setFigure(BLACK_KING);
		gameBoard[4][1].setFigure(BLACK_ROOK);
		gameBoard[0][3].setFigure(WHITE_QUEEN);
		setRountInfo();
		chess.move(4, 1, 4, 2);
		setRountInfo();
		boolean actual = CheckControllerService.isCheckAfterMove(chess, BLACK);
		assertTrue(actual);
	}

	@Test
	public void sholudReturnFalseWhenEnemyDontDoCheck() throws InvalidMoveException {
		chess.initGame(BLACK);
		gameBoard[5][3].setFigure(BLACK_KING);
		gameBoard[7][7].setFigure(WHITE_QUEEN);
		setRountInfo();
		boolean actual = CheckControllerService.isCheckAfterMove(chess, BLACK);
		assertFalse(actual);
	}

	@Test
	public void sholudReturnFalseWhenNoEnemyOnBoard() throws InvalidMoveException {
		chess.initGame(BLACK);
		gameBoard[5][3].setFigure(BLACK_KING);
		gameBoard[7][7].setFigure(BLACK_QUEEN);
		setRountInfo();
		boolean actual = CheckControllerService.isCheckAfterMove(chess, BLACK);
		assertFalse(actual);
	}

	private void setRountInfo() {
		FigureCountService.setCountOfFigures(chess, gameBoard);
		MoveValidationService.setPossibleMovesForAll(chess, gameBoard);
	}
}
