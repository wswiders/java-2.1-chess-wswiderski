package com.capgemini.chess.game.servieces;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.figure.impl.Pawn;
import com.capgemini.chess.game.exception.InvalidMoveException;
import com.capgemini.chess.game.impl.ChessImpl;
import com.capgemini.chess.game.servieces.FigureCountService;

public class FigureCountServiceTest {

	private ChessImpl chess;
	private Box[][] gameBoard;

	@Before
	public void setUp() throws Exception {
		chess = new ChessImpl();
		chess.initGame(Colour.WHITE);
	}

	@Test
	public void shouldReturn16WhiteFigures() throws InvalidMoveException {
		chess.placeFigures();
		gameBoard = chess.getGameBoard();
		FigureCountService.setCountOfFigures(chess, gameBoard);
		int actual = chess.getBoxWithWhiteFigures().size();
		assertEquals(16, actual);
	}

	@Test
	public void shouldReturn16BlackFigures() throws InvalidMoveException {
		chess.placeFigures();
		gameBoard = chess.getGameBoard();
		FigureCountService.setCountOfFigures(chess, gameBoard);
		int actual = chess.getBoxWithBlackFigures().size();
		assertEquals(16, actual);
	}

	@Test
	public void shouldReturnt0BlackFiguresWhenFiguresNotSet() throws InvalidMoveException {
		gameBoard = chess.getGameBoard();
		FigureCountService.setCountOfFigures(chess, gameBoard);
		int actual = chess.getBoxWithBlackFigures().size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldReturn0WhiteFiguresWhenFiguresNotSet() throws InvalidMoveException {
		gameBoard = chess.getGameBoard();
		FigureCountService.setCountOfFigures(chess, gameBoard);
		int actual = chess.getBoxWithWhiteFigures().size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldReturn1WhiteFiguresWhenFigureIsAdd() throws InvalidMoveException {
		gameBoard = chess.getGameBoard();
		gameBoard[0][0].setFigure(new Pawn(Colour.WHITE, new Box(1, 1)));
		FigureCountService.setCountOfFigures(chess, gameBoard);
		int actual = chess.getBoxWithWhiteFigures().size();
		assertEquals(1, actual);
	}
}
