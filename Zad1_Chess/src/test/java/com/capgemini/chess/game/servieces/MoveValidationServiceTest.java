package com.capgemini.chess.game.servieces;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.chess.board.Board;
import com.capgemini.chess.board.box.Box;
import com.capgemini.chess.figure.color.Colour;
import com.capgemini.chess.figure.impl.Bishop;
import com.capgemini.chess.figure.impl.King;
import com.capgemini.chess.figure.impl.Knight;
import com.capgemini.chess.figure.impl.Pawn;
import com.capgemini.chess.figure.impl.Queen;
import com.capgemini.chess.figure.impl.Rook;
import com.capgemini.chess.game.servieces.MoveValidationService;

public class MoveValidationServiceTest {

	private static final Queen BLACK_QUEEN = new Queen(Colour.BLACK, new Box(1, 1));
	private static final King BLACK_KING = new King(Colour.BLACK, new Box(1, 2));
	private static final Bishop BLACK_BISHOP = new Bishop(Colour.BLACK, new Box(1, 3));
	private static final Rook BLACK_ROOK = new Rook(Colour.BLACK, new Box(1, 4));
	private static final Knight BLACK_KNIGHT = new Knight(Colour.BLACK, new Box(1, 5));
	private static final Knight WHITE_KNIGHT = new Knight(Colour.WHITE, new Box(1, 6));
	private static final Pawn BLACK_PAWN = new Pawn(Colour.BLACK, new Box(1, 7));
	private static final Pawn WHITE_PAWN = new Pawn(Colour.WHITE, new Box(1, 0));
	private static final Pawn WHITE_PAWN_MOVE = new Pawn(Colour.WHITE, new Box(6, 0));
	private Board board;
	private Box[][] gameBoard;

	@Before
	public void setUp() throws Exception {
		board = new Board();
		gameBoard = board.getBoard();
	}

	@Test
	public void shouldReturnTwoBoxesPosibleWhenPawnFirstMove() {
		Box box = gameBoard[6][1];
		gameBoard[6][1].setFigure(WHITE_PAWN_MOVE);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(2, actual);
	}

	@Test
	public void shouldReturnOneBoxPosibleWhenPawnSecondMove() {
		Box box = gameBoard[6][1];
		box.setFigure(WHITE_PAWN);
		box.getFigure().addNumberOfMoves();
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(1, actual);
	}

	@Test
	public void should0BoxBePosibleForPawnWhenEnemyStandOnWay() {
		Box box = gameBoard[6][1];
		box.setFigure(WHITE_PAWN);
		gameBoard[5][1].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(0, actual);
	}

	@Test
	public void should0BoxBePosibleForPawnWhenAlliesIsOnWay() {
		Box box = gameBoard[6][1];
		box.setFigure(WHITE_PAWN);
		gameBoard[5][1].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(0, actual);
	}

	@Test
	public void should0BoxToAttackBePosibleWhenNoEnemy() {
		Box box = gameBoard[6][1];
		box.setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(0, actual);
	}

	@Test
	public void shouldOneBoxToAttackBePosibleForPawnWhenEnemyStadInAttackBox() {
		Box box = gameBoard[6][1];
		box.setFigure(WHITE_PAWN);
		gameBoard[5][2].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(1, actual);
	}

	@Test
	public void should8BoxToMoveBePosibleForKnight() {
		Box box = gameBoard[4][4];
		box.setFigure(WHITE_KNIGHT);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(8, actual);
	}

	@Test
	public void should7BoxToMoveBePosibleForKnightWhenAlliesOnWay() {
		Box box = gameBoard[4][4];
		box.setFigure(WHITE_KNIGHT);
		gameBoard[6][3].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(7, actual);
	}

	@Test
	public void should7BoxToMoveBePosibleForKnightWhenEnemyOnWay() {
		Box box = gameBoard[4][4];
		box.setFigure(BLACK_KNIGHT);
		gameBoard[6][3].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(7, actual);
	}

	@Test
	public void shouldOneBoxToAttackBePosibleForKnightWhenEnemyOnWay() {
		Box box = gameBoard[4][4];
		box.setFigure(BLACK_KNIGHT);
		gameBoard[6][3].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(1, actual);
	}

	@Test
	public void shouldAllBoxToAttackBePosibleForKnightWhenEnemyOnWay() {
		Box box = gameBoard[4][4];
		box.setFigure(BLACK_KNIGHT);
		gameBoard[6][3].setFigure(WHITE_PAWN);
		gameBoard[6][5].setFigure(WHITE_PAWN);
		gameBoard[2][3].setFigure(WHITE_PAWN);
		gameBoard[2][5].setFigure(WHITE_PAWN);
		gameBoard[5][2].setFigure(WHITE_PAWN);
		gameBoard[3][2].setFigure(WHITE_PAWN);
		gameBoard[5][6].setFigure(WHITE_PAWN);
		gameBoard[3][6].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(8, actual);
	}

	@Test
	public void should0BoxToMoveBePosibleForKnightWhenEnemyOnWay() {
		Box box = gameBoard[4][4];
		box.setFigure(BLACK_KNIGHT);
		gameBoard[6][3].setFigure(WHITE_PAWN);
		gameBoard[6][5].setFigure(WHITE_PAWN);
		gameBoard[2][3].setFigure(WHITE_PAWN);
		gameBoard[2][5].setFigure(WHITE_PAWN);
		gameBoard[5][2].setFigure(WHITE_PAWN);
		gameBoard[3][2].setFigure(WHITE_PAWN);
		gameBoard[5][6].setFigure(WHITE_PAWN);
		gameBoard[3][6].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(0, actual);
	}

	@Test
	public void should0BoxToMoveBePosibleForKnightWhenAlliesOnWay() {
		Box box = gameBoard[4][4];
		box.setFigure(BLACK_KNIGHT);
		gameBoard[6][3].setFigure(BLACK_PAWN);
		gameBoard[6][5].setFigure(BLACK_PAWN);
		gameBoard[2][3].setFigure(BLACK_PAWN);
		gameBoard[2][5].setFigure(BLACK_PAWN);
		gameBoard[5][2].setFigure(BLACK_PAWN);
		gameBoard[3][2].setFigure(BLACK_PAWN);
		gameBoard[5][6].setFigure(BLACK_PAWN);
		gameBoard[3][6].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(0, actual);
	}

	@Test
	public void should14BoxToMoveBePosibleForRook() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_ROOK);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(14, actual);
	}

	@Test
	public void should1BoxToMoveBePosibleForRookWhenAlliesOnWhay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_ROOK);
		gameBoard[5][7].setFigure(BLACK_PAWN);
		gameBoard[7][6].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(1, actual);
	}

	@Test
	public void should0BoxToMoveBePosibleForRookWhenAlliesOnWhay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_ROOK);
		gameBoard[6][7].setFigure(BLACK_PAWN);
		gameBoard[7][6].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(0, actual);
	}

	@Test
	public void should1BoxToMoveBePosibleForRookWhenAlliesAndEnemyOnWhay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_ROOK);
		gameBoard[5][7].setFigure(WHITE_PAWN);
		gameBoard[7][6].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(1, actual);
	}

	@Test
	public void should1BoxToAttackBePosibleForRookWhenAlliesOnWhay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_ROOK);
		gameBoard[5][7].setFigure(WHITE_PAWN);
		gameBoard[7][6].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(1, actual);
	}

	@Test
	public void should0BoxToAttackBePosibleForRook() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_ROOK);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(0, actual);
	}

	@Test
	public void should7BoxToMoveBePosibleForBishop() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_BISHOP);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(7, actual);
	}

	@Test
	public void should0BoxToAttackBePosibleForBishop() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_BISHOP);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(0, actual);
	}

	@Test
	public void should0BoxToMoveBePosibleForBishopWhenAlliesOnWay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_BISHOP);
		gameBoard[6][6].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(0, actual);
	}

	@Test
	public void should0BoxToMoveBePosibleForBishopWhenEnemyOnWay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_BISHOP);
		gameBoard[6][6].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(0, actual);
	}

	@Test
	public void should1BoxToAtaackBePosibleForBishopWhenEnemyOnWay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_BISHOP);
		gameBoard[6][6].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(1, actual);
	}

	@Test
	public void should8BoxToMoveBePosibleForKing() {
		Box box = gameBoard[5][5];
		box.setFigure(BLACK_KING);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(8, actual);
	}

	@Test
	public void shoul08BoxToAttackBePosibleForKing() {
		Box box = gameBoard[5][5];
		box.setFigure(BLACK_KING);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(0, actual);
	}

	@Test
	public void should6BoxToMoveBePosibleForKingWhenAlliesOnWay() {
		Box box = gameBoard[5][5];
		box.setFigure(BLACK_KING);
		gameBoard[4][4].setFigure(BLACK_PAWN);
		gameBoard[6][6].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(6, actual);
	}

	@Test
	public void should6BoxToMoveBePosibleForKingWhenEnemyOnWay() {
		Box box = gameBoard[5][5];
		box.setFigure(BLACK_KING);
		gameBoard[4][4].setFigure(WHITE_PAWN);
		gameBoard[6][6].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(6, actual);
	}

	@Test
	public void should6BoxToAtakBePosibleForKingWhenEnemyOnWay() {
		Box box = gameBoard[5][5];
		box.setFigure(BLACK_KING);
		gameBoard[4][4].setFigure(WHITE_PAWN);
		gameBoard[6][6].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(2, actual);
	}

	@Test
	public void should21BoxToMoveBePosibleForQueen() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_QUEEN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(21, actual);
	}

	@Test
	public void should14BoxToMoveBePosibleForQueenWhenAlliesOnWay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_QUEEN);
		gameBoard[7][6].setFigure(BLACK_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(14, actual);
	}

	@Test
	public void should14BoxToMoveBePosibleForQueenWhenEnemyOnWay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_QUEEN);
		gameBoard[7][6].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosiblePosition().size();
		assertEquals(14, actual);
	}

	@Test
	public void should1BoxToAttackBePosibleForQueenWhenEnemyOnWay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_QUEEN);
		gameBoard[7][6].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(1, actual);
	}

	@Test
	public void should1BoxToAttackBePosibleForQueenWhenEnemysOnWay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_QUEEN);
		gameBoard[7][6].setFigure(WHITE_PAWN);
		gameBoard[7][3].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(1, actual);
	}

	@Test
	public void should2BoxToAttackBePosibleForQueenWhenEnemysOnWay() {
		Box box = gameBoard[7][7];
		box.setFigure(BLACK_QUEEN);
		gameBoard[7][6].setFigure(WHITE_PAWN);
		gameBoard[3][7].setFigure(WHITE_PAWN);
		MoveValidationService.setPossibleMoves(box, gameBoard);
		int actual = box.getFigure().getPosibleToBeat().size();
		assertEquals(2, actual);
	}

}
